<?php
/**
* @package   frontend
* @subpackage bootstrap
* @author    Florian Gadal
* @copyright 2012 Coffee Ring Prod
* @link      http://coffeeringprod.fr
* @license    All rights reserved
*/

class navbarZone extends jZone {
    protected $_tplname='navbar';

    protected function _prepareTpl(){

		$navbarLinks = jEvent::notify('BootstrapGetNavbarContent')->getResponse();
		usort($navbarLinks, 'bootstrapNavbarLinkSort');

        $this->_tpl->assign('links', $navbarLinks);
    }
}
