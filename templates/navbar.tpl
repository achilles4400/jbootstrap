{foreach $links as $link}

<li{if $active == $link->id} class="active"{/if}>
	<a href="{$link->url}">{$link->label}</a>
</li>
{/foreach}
